/*
 * generated by Xtext 2.15.0
 */
package coursewebxtext.formatting2

import WebPageCourses.Course
import WebPageCourses.StudyProgram
import com.google.inject.Inject
import coursewebxtext.services.MyDslGrammarAccess
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument

class MyDslFormatter extends AbstractFormatter2 {
	
	@Inject extension MyDslGrammarAccess

	def dispatch void format(StudyProgram studyProgram, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (course : studyProgram.courses) {
			course.format
		}
	}

	def dispatch void format(Course course, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (courseInstance : course.instances) {
			courseInstance.format
		}
		course.coursework.format
		course.relatedcourses.format
	}
	
	// TODO: implement for CourseInstance, RelatedCourses, EvaluationForm, Organization, Timetable
}
