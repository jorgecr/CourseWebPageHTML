package coursewebxtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import coursewebxtext.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'StudyProgram'", "'{'", "'schedule'", "'('", "','", "')'", "'courses'", "'}'", "'Schedule'", "'date'", "'Time'", "'Room'", "'Course'", "'code'", "'content'", "'credits'", "'instances'", "'coursework'", "'relatedcourses'", "'CourseInstance'", "'semester'", "'evaluationform'", "'organizations'", "'timetable'", "'-'", "'.'", "'E'", "'e'", "'Coursework'", "'lectureHours'", "'labHours'", "'RelatedCourses'", "'requiredcourses'", "'reductioncourses'", "'EvaluationForm'", "'Work'", "'Organization'", "'department'", "'people'", "'Timetable'", "'porcentage'", "'People'", "'Role'", "'Requirement'", "'TypeOfRequierement'", "'CreditReductionCourse'", "'creditReduction'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "StudyProgram";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStudyProgram"
    // InternalMyDsl.g:64:1: entryRuleStudyProgram returns [EObject current=null] : iv_ruleStudyProgram= ruleStudyProgram EOF ;
    public final EObject entryRuleStudyProgram() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStudyProgram = null;


        try {
            // InternalMyDsl.g:64:53: (iv_ruleStudyProgram= ruleStudyProgram EOF )
            // InternalMyDsl.g:65:2: iv_ruleStudyProgram= ruleStudyProgram EOF
            {
             newCompositeNode(grammarAccess.getStudyProgramRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStudyProgram=ruleStudyProgram();

            state._fsp--;

             current =iv_ruleStudyProgram; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStudyProgram"


    // $ANTLR start "ruleStudyProgram"
    // InternalMyDsl.g:71:1: ruleStudyProgram returns [EObject current=null] : ( () otherlv_1= 'StudyProgram' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'schedule' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'courses' otherlv_11= '{' ( (lv_courses_12_0= ruleCourse ) ) (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )* otherlv_15= '}' )? otherlv_16= '}' ) ;
    public final EObject ruleStudyProgram() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_courses_12_0 = null;

        EObject lv_courses_14_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( ( () otherlv_1= 'StudyProgram' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'schedule' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'courses' otherlv_11= '{' ( (lv_courses_12_0= ruleCourse ) ) (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )* otherlv_15= '}' )? otherlv_16= '}' ) )
            // InternalMyDsl.g:78:2: ( () otherlv_1= 'StudyProgram' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'schedule' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'courses' otherlv_11= '{' ( (lv_courses_12_0= ruleCourse ) ) (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )* otherlv_15= '}' )? otherlv_16= '}' )
            {
            // InternalMyDsl.g:78:2: ( () otherlv_1= 'StudyProgram' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'schedule' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'courses' otherlv_11= '{' ( (lv_courses_12_0= ruleCourse ) ) (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )* otherlv_15= '}' )? otherlv_16= '}' )
            // InternalMyDsl.g:79:3: () otherlv_1= 'StudyProgram' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'schedule' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )? (otherlv_10= 'courses' otherlv_11= '{' ( (lv_courses_12_0= ruleCourse ) ) (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )* otherlv_15= '}' )? otherlv_16= '}'
            {
            // InternalMyDsl.g:79:3: ()
            // InternalMyDsl.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStudyProgramAccess().getStudyProgramAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getStudyProgramAccess().getStudyProgramKeyword_1());
            		
            // InternalMyDsl.g:90:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:91:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:91:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:92:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStudyProgramAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStudyProgramRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"coursewebxtext.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getStudyProgramAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMyDsl.g:113:3: (otherlv_4= 'schedule' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:114:4: otherlv_4= 'schedule' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_6); 

                    				newLeafNode(otherlv_4, grammarAccess.getStudyProgramAccess().getScheduleKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,14,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getStudyProgramAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalMyDsl.g:122:4: ( ( ruleEString ) )
                    // InternalMyDsl.g:123:5: ( ruleEString )
                    {
                    // InternalMyDsl.g:123:5: ( ruleEString )
                    // InternalMyDsl.g:124:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getStudyProgramRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getStudyProgramAccess().getScheduleScheduleCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_7);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:138:4: (otherlv_7= ',' ( ( ruleEString ) ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==15) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalMyDsl.g:139:5: otherlv_7= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_7=(Token)match(input,15,FOLLOW_3); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getStudyProgramAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalMyDsl.g:143:5: ( ( ruleEString ) )
                    	    // InternalMyDsl.g:144:6: ( ruleEString )
                    	    {
                    	    // InternalMyDsl.g:144:6: ( ruleEString )
                    	    // InternalMyDsl.g:145:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getStudyProgramRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getStudyProgramAccess().getScheduleScheduleCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_7);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,16,FOLLOW_8); 

                    				newLeafNode(otherlv_9, grammarAccess.getStudyProgramAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalMyDsl.g:165:3: (otherlv_10= 'courses' otherlv_11= '{' ( (lv_courses_12_0= ruleCourse ) ) (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )* otherlv_15= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:166:4: otherlv_10= 'courses' otherlv_11= '{' ( (lv_courses_12_0= ruleCourse ) ) (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )* otherlv_15= '}'
                    {
                    otherlv_10=(Token)match(input,17,FOLLOW_4); 

                    				newLeafNode(otherlv_10, grammarAccess.getStudyProgramAccess().getCoursesKeyword_5_0());
                    			
                    otherlv_11=(Token)match(input,12,FOLLOW_9); 

                    				newLeafNode(otherlv_11, grammarAccess.getStudyProgramAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalMyDsl.g:174:4: ( (lv_courses_12_0= ruleCourse ) )
                    // InternalMyDsl.g:175:5: (lv_courses_12_0= ruleCourse )
                    {
                    // InternalMyDsl.g:175:5: (lv_courses_12_0= ruleCourse )
                    // InternalMyDsl.g:176:6: lv_courses_12_0= ruleCourse
                    {

                    						newCompositeNode(grammarAccess.getStudyProgramAccess().getCoursesCourseParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_courses_12_0=ruleCourse();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStudyProgramRule());
                    						}
                    						add(
                    							current,
                    							"courses",
                    							lv_courses_12_0,
                    							"coursewebxtext.MyDsl.Course");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:193:4: (otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==15) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalMyDsl.g:194:5: otherlv_13= ',' ( (lv_courses_14_0= ruleCourse ) )
                    	    {
                    	    otherlv_13=(Token)match(input,15,FOLLOW_9); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getStudyProgramAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalMyDsl.g:198:5: ( (lv_courses_14_0= ruleCourse ) )
                    	    // InternalMyDsl.g:199:6: (lv_courses_14_0= ruleCourse )
                    	    {
                    	    // InternalMyDsl.g:199:6: (lv_courses_14_0= ruleCourse )
                    	    // InternalMyDsl.g:200:7: lv_courses_14_0= ruleCourse
                    	    {

                    	    							newCompositeNode(grammarAccess.getStudyProgramAccess().getCoursesCourseParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_courses_14_0=ruleCourse();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getStudyProgramRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"courses",
                    	    								lv_courses_14_0,
                    	    								"coursewebxtext.MyDsl.Course");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,18,FOLLOW_11); 

                    				newLeafNode(otherlv_15, grammarAccess.getStudyProgramAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            otherlv_16=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_16, grammarAccess.getStudyProgramAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStudyProgram"


    // $ANTLR start "entryRuleEString"
    // InternalMyDsl.g:231:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalMyDsl.g:231:47: (iv_ruleEString= ruleEString EOF )
            // InternalMyDsl.g:232:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalMyDsl.g:238:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:244:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalMyDsl.g:245:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalMyDsl.g:245:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_STRING) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_ID) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalMyDsl.g:246:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:254:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleSchedule"
    // InternalMyDsl.g:265:1: entryRuleSchedule returns [EObject current=null] : iv_ruleSchedule= ruleSchedule EOF ;
    public final EObject entryRuleSchedule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSchedule = null;


        try {
            // InternalMyDsl.g:265:49: (iv_ruleSchedule= ruleSchedule EOF )
            // InternalMyDsl.g:266:2: iv_ruleSchedule= ruleSchedule EOF
            {
             newCompositeNode(grammarAccess.getScheduleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSchedule=ruleSchedule();

            state._fsp--;

             current =iv_ruleSchedule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSchedule"


    // $ANTLR start "ruleSchedule"
    // InternalMyDsl.g:272:1: ruleSchedule returns [EObject current=null] : ( () otherlv_1= 'Schedule' otherlv_2= '{' (otherlv_3= 'date' ( (lv_date_4_0= ruleEString ) ) )? (otherlv_5= 'Time' ( (lv_Time_6_0= ruleEString ) ) )? (otherlv_7= 'Room' ( (lv_Room_8_0= ruleEString ) ) )? otherlv_9= '}' ) ;
    public final EObject ruleSchedule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_date_4_0 = null;

        AntlrDatatypeRuleToken lv_Time_6_0 = null;

        AntlrDatatypeRuleToken lv_Room_8_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:278:2: ( ( () otherlv_1= 'Schedule' otherlv_2= '{' (otherlv_3= 'date' ( (lv_date_4_0= ruleEString ) ) )? (otherlv_5= 'Time' ( (lv_Time_6_0= ruleEString ) ) )? (otherlv_7= 'Room' ( (lv_Room_8_0= ruleEString ) ) )? otherlv_9= '}' ) )
            // InternalMyDsl.g:279:2: ( () otherlv_1= 'Schedule' otherlv_2= '{' (otherlv_3= 'date' ( (lv_date_4_0= ruleEString ) ) )? (otherlv_5= 'Time' ( (lv_Time_6_0= ruleEString ) ) )? (otherlv_7= 'Room' ( (lv_Room_8_0= ruleEString ) ) )? otherlv_9= '}' )
            {
            // InternalMyDsl.g:279:2: ( () otherlv_1= 'Schedule' otherlv_2= '{' (otherlv_3= 'date' ( (lv_date_4_0= ruleEString ) ) )? (otherlv_5= 'Time' ( (lv_Time_6_0= ruleEString ) ) )? (otherlv_7= 'Room' ( (lv_Room_8_0= ruleEString ) ) )? otherlv_9= '}' )
            // InternalMyDsl.g:280:3: () otherlv_1= 'Schedule' otherlv_2= '{' (otherlv_3= 'date' ( (lv_date_4_0= ruleEString ) ) )? (otherlv_5= 'Time' ( (lv_Time_6_0= ruleEString ) ) )? (otherlv_7= 'Room' ( (lv_Room_8_0= ruleEString ) ) )? otherlv_9= '}'
            {
            // InternalMyDsl.g:280:3: ()
            // InternalMyDsl.g:281:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getScheduleAccess().getScheduleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getScheduleAccess().getScheduleKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getScheduleAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:295:3: (otherlv_3= 'date' ( (lv_date_4_0= ruleEString ) ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==20) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalMyDsl.g:296:4: otherlv_3= 'date' ( (lv_date_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,20,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getScheduleAccess().getDateKeyword_3_0());
                    			
                    // InternalMyDsl.g:300:4: ( (lv_date_4_0= ruleEString ) )
                    // InternalMyDsl.g:301:5: (lv_date_4_0= ruleEString )
                    {
                    // InternalMyDsl.g:301:5: (lv_date_4_0= ruleEString )
                    // InternalMyDsl.g:302:6: lv_date_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getScheduleAccess().getDateEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_13);
                    lv_date_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScheduleRule());
                    						}
                    						set(
                    							current,
                    							"date",
                    							lv_date_4_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:320:3: (otherlv_5= 'Time' ( (lv_Time_6_0= ruleEString ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalMyDsl.g:321:4: otherlv_5= 'Time' ( (lv_Time_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,21,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getScheduleAccess().getTimeKeyword_4_0());
                    			
                    // InternalMyDsl.g:325:4: ( (lv_Time_6_0= ruleEString ) )
                    // InternalMyDsl.g:326:5: (lv_Time_6_0= ruleEString )
                    {
                    // InternalMyDsl.g:326:5: (lv_Time_6_0= ruleEString )
                    // InternalMyDsl.g:327:6: lv_Time_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getScheduleAccess().getTimeEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_Time_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScheduleRule());
                    						}
                    						set(
                    							current,
                    							"Time",
                    							lv_Time_6_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:345:3: (otherlv_7= 'Room' ( (lv_Room_8_0= ruleEString ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==22) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalMyDsl.g:346:4: otherlv_7= 'Room' ( (lv_Room_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,22,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getScheduleAccess().getRoomKeyword_5_0());
                    			
                    // InternalMyDsl.g:350:4: ( (lv_Room_8_0= ruleEString ) )
                    // InternalMyDsl.g:351:5: (lv_Room_8_0= ruleEString )
                    {
                    // InternalMyDsl.g:351:5: (lv_Room_8_0= ruleEString )
                    // InternalMyDsl.g:352:6: lv_Room_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getScheduleAccess().getRoomEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_Room_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScheduleRule());
                    						}
                    						set(
                    							current,
                    							"Room",
                    							lv_Room_8_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getScheduleAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSchedule"


    // $ANTLR start "entryRuleCourse"
    // InternalMyDsl.g:378:1: entryRuleCourse returns [EObject current=null] : iv_ruleCourse= ruleCourse EOF ;
    public final EObject entryRuleCourse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCourse = null;


        try {
            // InternalMyDsl.g:378:47: (iv_ruleCourse= ruleCourse EOF )
            // InternalMyDsl.g:379:2: iv_ruleCourse= ruleCourse EOF
            {
             newCompositeNode(grammarAccess.getCourseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCourse=ruleCourse();

            state._fsp--;

             current =iv_ruleCourse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCourse"


    // $ANTLR start "ruleCourse"
    // InternalMyDsl.g:385:1: ruleCourse returns [EObject current=null] : ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'content' ( (lv_content_7_0= ruleEString ) ) )? (otherlv_8= 'credits' ( (lv_credits_9_0= ruleEDouble ) ) )? (otherlv_10= 'instances' otherlv_11= '{' ( (lv_instances_12_0= ruleCourseInstance ) ) (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )* otherlv_15= '}' )? (otherlv_16= 'coursework' ( (lv_coursework_17_0= ruleCoursework ) ) )? (otherlv_18= 'relatedcourses' ( (lv_relatedcourses_19_0= ruleRelatedCourses ) ) )? otherlv_20= '}' ) ;
    public final EObject ruleCourse() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_code_5_0 = null;

        AntlrDatatypeRuleToken lv_content_7_0 = null;

        AntlrDatatypeRuleToken lv_credits_9_0 = null;

        EObject lv_instances_12_0 = null;

        EObject lv_instances_14_0 = null;

        EObject lv_coursework_17_0 = null;

        EObject lv_relatedcourses_19_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:391:2: ( ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'content' ( (lv_content_7_0= ruleEString ) ) )? (otherlv_8= 'credits' ( (lv_credits_9_0= ruleEDouble ) ) )? (otherlv_10= 'instances' otherlv_11= '{' ( (lv_instances_12_0= ruleCourseInstance ) ) (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )* otherlv_15= '}' )? (otherlv_16= 'coursework' ( (lv_coursework_17_0= ruleCoursework ) ) )? (otherlv_18= 'relatedcourses' ( (lv_relatedcourses_19_0= ruleRelatedCourses ) ) )? otherlv_20= '}' ) )
            // InternalMyDsl.g:392:2: ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'content' ( (lv_content_7_0= ruleEString ) ) )? (otherlv_8= 'credits' ( (lv_credits_9_0= ruleEDouble ) ) )? (otherlv_10= 'instances' otherlv_11= '{' ( (lv_instances_12_0= ruleCourseInstance ) ) (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )* otherlv_15= '}' )? (otherlv_16= 'coursework' ( (lv_coursework_17_0= ruleCoursework ) ) )? (otherlv_18= 'relatedcourses' ( (lv_relatedcourses_19_0= ruleRelatedCourses ) ) )? otherlv_20= '}' )
            {
            // InternalMyDsl.g:392:2: ( () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'content' ( (lv_content_7_0= ruleEString ) ) )? (otherlv_8= 'credits' ( (lv_credits_9_0= ruleEDouble ) ) )? (otherlv_10= 'instances' otherlv_11= '{' ( (lv_instances_12_0= ruleCourseInstance ) ) (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )* otherlv_15= '}' )? (otherlv_16= 'coursework' ( (lv_coursework_17_0= ruleCoursework ) ) )? (otherlv_18= 'relatedcourses' ( (lv_relatedcourses_19_0= ruleRelatedCourses ) ) )? otherlv_20= '}' )
            // InternalMyDsl.g:393:3: () otherlv_1= 'Course' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'content' ( (lv_content_7_0= ruleEString ) ) )? (otherlv_8= 'credits' ( (lv_credits_9_0= ruleEDouble ) ) )? (otherlv_10= 'instances' otherlv_11= '{' ( (lv_instances_12_0= ruleCourseInstance ) ) (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )* otherlv_15= '}' )? (otherlv_16= 'coursework' ( (lv_coursework_17_0= ruleCoursework ) ) )? (otherlv_18= 'relatedcourses' ( (lv_relatedcourses_19_0= ruleRelatedCourses ) ) )? otherlv_20= '}'
            {
            // InternalMyDsl.g:393:3: ()
            // InternalMyDsl.g:394:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCourseAccess().getCourseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getCourseAccess().getCourseKeyword_1());
            		
            // InternalMyDsl.g:404:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:405:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:405:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:406:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCourseAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCourseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"coursewebxtext.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getCourseAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMyDsl.g:427:3: (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==24) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:428:4: otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getCourseAccess().getCodeKeyword_4_0());
                    			
                    // InternalMyDsl.g:432:4: ( (lv_code_5_0= ruleEString ) )
                    // InternalMyDsl.g:433:5: (lv_code_5_0= ruleEString )
                    {
                    // InternalMyDsl.g:433:5: (lv_code_5_0= ruleEString )
                    // InternalMyDsl.g:434:6: lv_code_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getCodeEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_16);
                    lv_code_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						set(
                    							current,
                    							"code",
                    							lv_code_5_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:452:3: (otherlv_6= 'content' ( (lv_content_7_0= ruleEString ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==25) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalMyDsl.g:453:4: otherlv_6= 'content' ( (lv_content_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,25,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getCourseAccess().getContentKeyword_5_0());
                    			
                    // InternalMyDsl.g:457:4: ( (lv_content_7_0= ruleEString ) )
                    // InternalMyDsl.g:458:5: (lv_content_7_0= ruleEString )
                    {
                    // InternalMyDsl.g:458:5: (lv_content_7_0= ruleEString )
                    // InternalMyDsl.g:459:6: lv_content_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getContentEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_content_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						set(
                    							current,
                    							"content",
                    							lv_content_7_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:477:3: (otherlv_8= 'credits' ( (lv_credits_9_0= ruleEDouble ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==26) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalMyDsl.g:478:4: otherlv_8= 'credits' ( (lv_credits_9_0= ruleEDouble ) )
                    {
                    otherlv_8=(Token)match(input,26,FOLLOW_18); 

                    				newLeafNode(otherlv_8, grammarAccess.getCourseAccess().getCreditsKeyword_6_0());
                    			
                    // InternalMyDsl.g:482:4: ( (lv_credits_9_0= ruleEDouble ) )
                    // InternalMyDsl.g:483:5: (lv_credits_9_0= ruleEDouble )
                    {
                    // InternalMyDsl.g:483:5: (lv_credits_9_0= ruleEDouble )
                    // InternalMyDsl.g:484:6: lv_credits_9_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getCreditsEDoubleParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    lv_credits_9_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						set(
                    							current,
                    							"credits",
                    							lv_credits_9_0,
                    							"coursewebxtext.MyDsl.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:502:3: (otherlv_10= 'instances' otherlv_11= '{' ( (lv_instances_12_0= ruleCourseInstance ) ) (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )* otherlv_15= '}' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==27) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalMyDsl.g:503:4: otherlv_10= 'instances' otherlv_11= '{' ( (lv_instances_12_0= ruleCourseInstance ) ) (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )* otherlv_15= '}'
                    {
                    otherlv_10=(Token)match(input,27,FOLLOW_4); 

                    				newLeafNode(otherlv_10, grammarAccess.getCourseAccess().getInstancesKeyword_7_0());
                    			
                    otherlv_11=(Token)match(input,12,FOLLOW_20); 

                    				newLeafNode(otherlv_11, grammarAccess.getCourseAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalMyDsl.g:511:4: ( (lv_instances_12_0= ruleCourseInstance ) )
                    // InternalMyDsl.g:512:5: (lv_instances_12_0= ruleCourseInstance )
                    {
                    // InternalMyDsl.g:512:5: (lv_instances_12_0= ruleCourseInstance )
                    // InternalMyDsl.g:513:6: lv_instances_12_0= ruleCourseInstance
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getInstancesCourseInstanceParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_instances_12_0=ruleCourseInstance();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						add(
                    							current,
                    							"instances",
                    							lv_instances_12_0,
                    							"coursewebxtext.MyDsl.CourseInstance");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:530:4: (otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==15) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalMyDsl.g:531:5: otherlv_13= ',' ( (lv_instances_14_0= ruleCourseInstance ) )
                    	    {
                    	    otherlv_13=(Token)match(input,15,FOLLOW_20); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getCourseAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalMyDsl.g:535:5: ( (lv_instances_14_0= ruleCourseInstance ) )
                    	    // InternalMyDsl.g:536:6: (lv_instances_14_0= ruleCourseInstance )
                    	    {
                    	    // InternalMyDsl.g:536:6: (lv_instances_14_0= ruleCourseInstance )
                    	    // InternalMyDsl.g:537:7: lv_instances_14_0= ruleCourseInstance
                    	    {

                    	    							newCompositeNode(grammarAccess.getCourseAccess().getInstancesCourseInstanceParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_instances_14_0=ruleCourseInstance();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getCourseRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"instances",
                    	    								lv_instances_14_0,
                    	    								"coursewebxtext.MyDsl.CourseInstance");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,18,FOLLOW_21); 

                    				newLeafNode(otherlv_15, grammarAccess.getCourseAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            // InternalMyDsl.g:560:3: (otherlv_16= 'coursework' ( (lv_coursework_17_0= ruleCoursework ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==28) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalMyDsl.g:561:4: otherlv_16= 'coursework' ( (lv_coursework_17_0= ruleCoursework ) )
                    {
                    otherlv_16=(Token)match(input,28,FOLLOW_22); 

                    				newLeafNode(otherlv_16, grammarAccess.getCourseAccess().getCourseworkKeyword_8_0());
                    			
                    // InternalMyDsl.g:565:4: ( (lv_coursework_17_0= ruleCoursework ) )
                    // InternalMyDsl.g:566:5: (lv_coursework_17_0= ruleCoursework )
                    {
                    // InternalMyDsl.g:566:5: (lv_coursework_17_0= ruleCoursework )
                    // InternalMyDsl.g:567:6: lv_coursework_17_0= ruleCoursework
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getCourseworkCourseworkParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_23);
                    lv_coursework_17_0=ruleCoursework();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						set(
                    							current,
                    							"coursework",
                    							lv_coursework_17_0,
                    							"coursewebxtext.MyDsl.Coursework");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:585:3: (otherlv_18= 'relatedcourses' ( (lv_relatedcourses_19_0= ruleRelatedCourses ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==29) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalMyDsl.g:586:4: otherlv_18= 'relatedcourses' ( (lv_relatedcourses_19_0= ruleRelatedCourses ) )
                    {
                    otherlv_18=(Token)match(input,29,FOLLOW_24); 

                    				newLeafNode(otherlv_18, grammarAccess.getCourseAccess().getRelatedcoursesKeyword_9_0());
                    			
                    // InternalMyDsl.g:590:4: ( (lv_relatedcourses_19_0= ruleRelatedCourses ) )
                    // InternalMyDsl.g:591:5: (lv_relatedcourses_19_0= ruleRelatedCourses )
                    {
                    // InternalMyDsl.g:591:5: (lv_relatedcourses_19_0= ruleRelatedCourses )
                    // InternalMyDsl.g:592:6: lv_relatedcourses_19_0= ruleRelatedCourses
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getRelatedcoursesRelatedCoursesParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_relatedcourses_19_0=ruleRelatedCourses();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						set(
                    							current,
                    							"relatedcourses",
                    							lv_relatedcourses_19_0,
                    							"coursewebxtext.MyDsl.RelatedCourses");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_20=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_20, grammarAccess.getCourseAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCourse"


    // $ANTLR start "entryRuleCourseInstance"
    // InternalMyDsl.g:618:1: entryRuleCourseInstance returns [EObject current=null] : iv_ruleCourseInstance= ruleCourseInstance EOF ;
    public final EObject entryRuleCourseInstance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCourseInstance = null;


        try {
            // InternalMyDsl.g:618:55: (iv_ruleCourseInstance= ruleCourseInstance EOF )
            // InternalMyDsl.g:619:2: iv_ruleCourseInstance= ruleCourseInstance EOF
            {
             newCompositeNode(grammarAccess.getCourseInstanceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCourseInstance=ruleCourseInstance();

            state._fsp--;

             current =iv_ruleCourseInstance; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCourseInstance"


    // $ANTLR start "ruleCourseInstance"
    // InternalMyDsl.g:625:1: ruleCourseInstance returns [EObject current=null] : ( () otherlv_1= 'CourseInstance' otherlv_2= '{' (otherlv_3= 'semester' ( (lv_semester_4_0= ruleEString ) ) )? (otherlv_5= 'evaluationform' ( (lv_evaluationform_6_0= ruleEvaluationForm ) ) )? (otherlv_7= 'organizations' otherlv_8= '{' ( (lv_organizations_9_0= ruleOrganization ) ) (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )* otherlv_12= '}' )? (otherlv_13= 'timetable' ( (lv_timetable_14_0= ruleTimetable ) ) )? otherlv_15= '}' ) ;
    public final EObject ruleCourseInstance() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_semester_4_0 = null;

        EObject lv_evaluationform_6_0 = null;

        EObject lv_organizations_9_0 = null;

        EObject lv_organizations_11_0 = null;

        EObject lv_timetable_14_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:631:2: ( ( () otherlv_1= 'CourseInstance' otherlv_2= '{' (otherlv_3= 'semester' ( (lv_semester_4_0= ruleEString ) ) )? (otherlv_5= 'evaluationform' ( (lv_evaluationform_6_0= ruleEvaluationForm ) ) )? (otherlv_7= 'organizations' otherlv_8= '{' ( (lv_organizations_9_0= ruleOrganization ) ) (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )* otherlv_12= '}' )? (otherlv_13= 'timetable' ( (lv_timetable_14_0= ruleTimetable ) ) )? otherlv_15= '}' ) )
            // InternalMyDsl.g:632:2: ( () otherlv_1= 'CourseInstance' otherlv_2= '{' (otherlv_3= 'semester' ( (lv_semester_4_0= ruleEString ) ) )? (otherlv_5= 'evaluationform' ( (lv_evaluationform_6_0= ruleEvaluationForm ) ) )? (otherlv_7= 'organizations' otherlv_8= '{' ( (lv_organizations_9_0= ruleOrganization ) ) (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )* otherlv_12= '}' )? (otherlv_13= 'timetable' ( (lv_timetable_14_0= ruleTimetable ) ) )? otherlv_15= '}' )
            {
            // InternalMyDsl.g:632:2: ( () otherlv_1= 'CourseInstance' otherlv_2= '{' (otherlv_3= 'semester' ( (lv_semester_4_0= ruleEString ) ) )? (otherlv_5= 'evaluationform' ( (lv_evaluationform_6_0= ruleEvaluationForm ) ) )? (otherlv_7= 'organizations' otherlv_8= '{' ( (lv_organizations_9_0= ruleOrganization ) ) (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )* otherlv_12= '}' )? (otherlv_13= 'timetable' ( (lv_timetable_14_0= ruleTimetable ) ) )? otherlv_15= '}' )
            // InternalMyDsl.g:633:3: () otherlv_1= 'CourseInstance' otherlv_2= '{' (otherlv_3= 'semester' ( (lv_semester_4_0= ruleEString ) ) )? (otherlv_5= 'evaluationform' ( (lv_evaluationform_6_0= ruleEvaluationForm ) ) )? (otherlv_7= 'organizations' otherlv_8= '{' ( (lv_organizations_9_0= ruleOrganization ) ) (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )* otherlv_12= '}' )? (otherlv_13= 'timetable' ( (lv_timetable_14_0= ruleTimetable ) ) )? otherlv_15= '}'
            {
            // InternalMyDsl.g:633:3: ()
            // InternalMyDsl.g:634:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCourseInstanceAccess().getCourseInstanceAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,30,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getCourseInstanceAccess().getCourseInstanceKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_25); 

            			newLeafNode(otherlv_2, grammarAccess.getCourseInstanceAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:648:3: (otherlv_3= 'semester' ( (lv_semester_4_0= ruleEString ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==31) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalMyDsl.g:649:4: otherlv_3= 'semester' ( (lv_semester_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,31,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getCourseInstanceAccess().getSemesterKeyword_3_0());
                    			
                    // InternalMyDsl.g:653:4: ( (lv_semester_4_0= ruleEString ) )
                    // InternalMyDsl.g:654:5: (lv_semester_4_0= ruleEString )
                    {
                    // InternalMyDsl.g:654:5: (lv_semester_4_0= ruleEString )
                    // InternalMyDsl.g:655:6: lv_semester_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCourseInstanceAccess().getSemesterEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_26);
                    lv_semester_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseInstanceRule());
                    						}
                    						set(
                    							current,
                    							"semester",
                    							lv_semester_4_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:673:3: (otherlv_5= 'evaluationform' ( (lv_evaluationform_6_0= ruleEvaluationForm ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==32) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalMyDsl.g:674:4: otherlv_5= 'evaluationform' ( (lv_evaluationform_6_0= ruleEvaluationForm ) )
                    {
                    otherlv_5=(Token)match(input,32,FOLLOW_27); 

                    				newLeafNode(otherlv_5, grammarAccess.getCourseInstanceAccess().getEvaluationformKeyword_4_0());
                    			
                    // InternalMyDsl.g:678:4: ( (lv_evaluationform_6_0= ruleEvaluationForm ) )
                    // InternalMyDsl.g:679:5: (lv_evaluationform_6_0= ruleEvaluationForm )
                    {
                    // InternalMyDsl.g:679:5: (lv_evaluationform_6_0= ruleEvaluationForm )
                    // InternalMyDsl.g:680:6: lv_evaluationform_6_0= ruleEvaluationForm
                    {

                    						newCompositeNode(grammarAccess.getCourseInstanceAccess().getEvaluationformEvaluationFormParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_evaluationform_6_0=ruleEvaluationForm();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseInstanceRule());
                    						}
                    						set(
                    							current,
                    							"evaluationform",
                    							lv_evaluationform_6_0,
                    							"coursewebxtext.MyDsl.EvaluationForm");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:698:3: (otherlv_7= 'organizations' otherlv_8= '{' ( (lv_organizations_9_0= ruleOrganization ) ) (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )* otherlv_12= '}' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==33) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalMyDsl.g:699:4: otherlv_7= 'organizations' otherlv_8= '{' ( (lv_organizations_9_0= ruleOrganization ) ) (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )* otherlv_12= '}'
                    {
                    otherlv_7=(Token)match(input,33,FOLLOW_4); 

                    				newLeafNode(otherlv_7, grammarAccess.getCourseInstanceAccess().getOrganizationsKeyword_5_0());
                    			
                    otherlv_8=(Token)match(input,12,FOLLOW_29); 

                    				newLeafNode(otherlv_8, grammarAccess.getCourseInstanceAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalMyDsl.g:707:4: ( (lv_organizations_9_0= ruleOrganization ) )
                    // InternalMyDsl.g:708:5: (lv_organizations_9_0= ruleOrganization )
                    {
                    // InternalMyDsl.g:708:5: (lv_organizations_9_0= ruleOrganization )
                    // InternalMyDsl.g:709:6: lv_organizations_9_0= ruleOrganization
                    {

                    						newCompositeNode(grammarAccess.getCourseInstanceAccess().getOrganizationsOrganizationParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_organizations_9_0=ruleOrganization();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseInstanceRule());
                    						}
                    						add(
                    							current,
                    							"organizations",
                    							lv_organizations_9_0,
                    							"coursewebxtext.MyDsl.Organization");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:726:4: (otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==15) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalMyDsl.g:727:5: otherlv_10= ',' ( (lv_organizations_11_0= ruleOrganization ) )
                    	    {
                    	    otherlv_10=(Token)match(input,15,FOLLOW_29); 

                    	    					newLeafNode(otherlv_10, grammarAccess.getCourseInstanceAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalMyDsl.g:731:5: ( (lv_organizations_11_0= ruleOrganization ) )
                    	    // InternalMyDsl.g:732:6: (lv_organizations_11_0= ruleOrganization )
                    	    {
                    	    // InternalMyDsl.g:732:6: (lv_organizations_11_0= ruleOrganization )
                    	    // InternalMyDsl.g:733:7: lv_organizations_11_0= ruleOrganization
                    	    {

                    	    							newCompositeNode(grammarAccess.getCourseInstanceAccess().getOrganizationsOrganizationParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_organizations_11_0=ruleOrganization();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getCourseInstanceRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"organizations",
                    	    								lv_organizations_11_0,
                    	    								"coursewebxtext.MyDsl.Organization");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    otherlv_12=(Token)match(input,18,FOLLOW_30); 

                    				newLeafNode(otherlv_12, grammarAccess.getCourseInstanceAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalMyDsl.g:756:3: (otherlv_13= 'timetable' ( (lv_timetable_14_0= ruleTimetable ) ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==34) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalMyDsl.g:757:4: otherlv_13= 'timetable' ( (lv_timetable_14_0= ruleTimetable ) )
                    {
                    otherlv_13=(Token)match(input,34,FOLLOW_31); 

                    				newLeafNode(otherlv_13, grammarAccess.getCourseInstanceAccess().getTimetableKeyword_6_0());
                    			
                    // InternalMyDsl.g:761:4: ( (lv_timetable_14_0= ruleTimetable ) )
                    // InternalMyDsl.g:762:5: (lv_timetable_14_0= ruleTimetable )
                    {
                    // InternalMyDsl.g:762:5: (lv_timetable_14_0= ruleTimetable )
                    // InternalMyDsl.g:763:6: lv_timetable_14_0= ruleTimetable
                    {

                    						newCompositeNode(grammarAccess.getCourseInstanceAccess().getTimetableTimetableParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_timetable_14_0=ruleTimetable();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseInstanceRule());
                    						}
                    						set(
                    							current,
                    							"timetable",
                    							lv_timetable_14_0,
                    							"coursewebxtext.MyDsl.Timetable");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_15=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getCourseInstanceAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCourseInstance"


    // $ANTLR start "entryRuleEDouble"
    // InternalMyDsl.g:789:1: entryRuleEDouble returns [String current=null] : iv_ruleEDouble= ruleEDouble EOF ;
    public final String entryRuleEDouble() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEDouble = null;


        try {
            // InternalMyDsl.g:789:47: (iv_ruleEDouble= ruleEDouble EOF )
            // InternalMyDsl.g:790:2: iv_ruleEDouble= ruleEDouble EOF
            {
             newCompositeNode(grammarAccess.getEDoubleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEDouble=ruleEDouble();

            state._fsp--;

             current =iv_ruleEDouble.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEDouble"


    // $ANTLR start "ruleEDouble"
    // InternalMyDsl.g:796:1: ruleEDouble returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEDouble() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;
        Token this_INT_3=null;
        Token this_INT_7=null;


        	enterRule();

        try {
            // InternalMyDsl.g:802:2: ( ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) )
            // InternalMyDsl.g:803:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            {
            // InternalMyDsl.g:803:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            // InternalMyDsl.g:804:3: (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            {
            // InternalMyDsl.g:804:3: (kw= '-' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==35) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalMyDsl.g:805:4: kw= '-'
                    {
                    kw=(Token)match(input,35,FOLLOW_32); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEDoubleAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            // InternalMyDsl.g:811:3: (this_INT_1= RULE_INT )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==RULE_INT) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalMyDsl.g:812:4: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_33); 

                    				current.merge(this_INT_1);
                    			

                    				newLeafNode(this_INT_1, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_1());
                    			

                    }
                    break;

            }

            kw=(Token)match(input,36,FOLLOW_34); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getEDoubleAccess().getFullStopKeyword_2());
            		
            this_INT_3=(Token)match(input,RULE_INT,FOLLOW_35); 

            			current.merge(this_INT_3);
            		

            			newLeafNode(this_INT_3, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_3());
            		
            // InternalMyDsl.g:832:3: ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=37 && LA25_0<=38)) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalMyDsl.g:833:4: (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT
                    {
                    // InternalMyDsl.g:833:4: (kw= 'E' | kw= 'e' )
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==37) ) {
                        alt23=1;
                    }
                    else if ( (LA23_0==38) ) {
                        alt23=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 23, 0, input);

                        throw nvae;
                    }
                    switch (alt23) {
                        case 1 :
                            // InternalMyDsl.g:834:5: kw= 'E'
                            {
                            kw=(Token)match(input,37,FOLLOW_36); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_4_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalMyDsl.g:840:5: kw= 'e'
                            {
                            kw=(Token)match(input,38,FOLLOW_36); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_4_0_1());
                            				

                            }
                            break;

                    }

                    // InternalMyDsl.g:846:4: (kw= '-' )?
                    int alt24=2;
                    int LA24_0 = input.LA(1);

                    if ( (LA24_0==35) ) {
                        alt24=1;
                    }
                    switch (alt24) {
                        case 1 :
                            // InternalMyDsl.g:847:5: kw= '-'
                            {
                            kw=(Token)match(input,35,FOLLOW_34); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getHyphenMinusKeyword_4_1());
                            				

                            }
                            break;

                    }

                    this_INT_7=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_7);
                    			

                    				newLeafNode(this_INT_7, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_4_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEDouble"


    // $ANTLR start "entryRuleCoursework"
    // InternalMyDsl.g:865:1: entryRuleCoursework returns [EObject current=null] : iv_ruleCoursework= ruleCoursework EOF ;
    public final EObject entryRuleCoursework() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCoursework = null;


        try {
            // InternalMyDsl.g:865:51: (iv_ruleCoursework= ruleCoursework EOF )
            // InternalMyDsl.g:866:2: iv_ruleCoursework= ruleCoursework EOF
            {
             newCompositeNode(grammarAccess.getCourseworkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCoursework=ruleCoursework();

            state._fsp--;

             current =iv_ruleCoursework; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCoursework"


    // $ANTLR start "ruleCoursework"
    // InternalMyDsl.g:872:1: ruleCoursework returns [EObject current=null] : ( () otherlv_1= 'Coursework' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleCoursework() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_lectureHours_4_0 = null;

        AntlrDatatypeRuleToken lv_labHours_6_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:878:2: ( ( () otherlv_1= 'Coursework' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? otherlv_7= '}' ) )
            // InternalMyDsl.g:879:2: ( () otherlv_1= 'Coursework' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? otherlv_7= '}' )
            {
            // InternalMyDsl.g:879:2: ( () otherlv_1= 'Coursework' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? otherlv_7= '}' )
            // InternalMyDsl.g:880:3: () otherlv_1= 'Coursework' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? otherlv_7= '}'
            {
            // InternalMyDsl.g:880:3: ()
            // InternalMyDsl.g:881:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCourseworkAccess().getCourseworkAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,39,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getCourseworkAccess().getCourseworkKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_37); 

            			newLeafNode(otherlv_2, grammarAccess.getCourseworkAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:895:3: (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==40) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalMyDsl.g:896:4: otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) )
                    {
                    otherlv_3=(Token)match(input,40,FOLLOW_18); 

                    				newLeafNode(otherlv_3, grammarAccess.getCourseworkAccess().getLectureHoursKeyword_3_0());
                    			
                    // InternalMyDsl.g:900:4: ( (lv_lectureHours_4_0= ruleEDouble ) )
                    // InternalMyDsl.g:901:5: (lv_lectureHours_4_0= ruleEDouble )
                    {
                    // InternalMyDsl.g:901:5: (lv_lectureHours_4_0= ruleEDouble )
                    // InternalMyDsl.g:902:6: lv_lectureHours_4_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getCourseworkAccess().getLectureHoursEDoubleParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_38);
                    lv_lectureHours_4_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseworkRule());
                    						}
                    						set(
                    							current,
                    							"lectureHours",
                    							lv_lectureHours_4_0,
                    							"coursewebxtext.MyDsl.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:920:3: (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==41) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalMyDsl.g:921:4: otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) )
                    {
                    otherlv_5=(Token)match(input,41,FOLLOW_18); 

                    				newLeafNode(otherlv_5, grammarAccess.getCourseworkAccess().getLabHoursKeyword_4_0());
                    			
                    // InternalMyDsl.g:925:4: ( (lv_labHours_6_0= ruleEDouble ) )
                    // InternalMyDsl.g:926:5: (lv_labHours_6_0= ruleEDouble )
                    {
                    // InternalMyDsl.g:926:5: (lv_labHours_6_0= ruleEDouble )
                    // InternalMyDsl.g:927:6: lv_labHours_6_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getCourseworkAccess().getLabHoursEDoubleParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_labHours_6_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseworkRule());
                    						}
                    						set(
                    							current,
                    							"labHours",
                    							lv_labHours_6_0,
                    							"coursewebxtext.MyDsl.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getCourseworkAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCoursework"


    // $ANTLR start "entryRuleRelatedCourses"
    // InternalMyDsl.g:953:1: entryRuleRelatedCourses returns [EObject current=null] : iv_ruleRelatedCourses= ruleRelatedCourses EOF ;
    public final EObject entryRuleRelatedCourses() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelatedCourses = null;


        try {
            // InternalMyDsl.g:953:55: (iv_ruleRelatedCourses= ruleRelatedCourses EOF )
            // InternalMyDsl.g:954:2: iv_ruleRelatedCourses= ruleRelatedCourses EOF
            {
             newCompositeNode(grammarAccess.getRelatedCoursesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelatedCourses=ruleRelatedCourses();

            state._fsp--;

             current =iv_ruleRelatedCourses; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelatedCourses"


    // $ANTLR start "ruleRelatedCourses"
    // InternalMyDsl.g:960:1: ruleRelatedCourses returns [EObject current=null] : ( () otherlv_1= 'RelatedCourses' otherlv_2= '{' (otherlv_3= 'requiredcourses' otherlv_4= '{' ( (lv_requiredcourses_5_0= ruleRequirement ) ) (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )* otherlv_8= '}' )? (otherlv_9= 'reductioncourses' otherlv_10= '{' ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) ) (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) ;
    public final EObject ruleRelatedCourses() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        EObject lv_requiredcourses_5_0 = null;

        EObject lv_requiredcourses_7_0 = null;

        EObject lv_reductioncourses_11_0 = null;

        EObject lv_reductioncourses_13_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:966:2: ( ( () otherlv_1= 'RelatedCourses' otherlv_2= '{' (otherlv_3= 'requiredcourses' otherlv_4= '{' ( (lv_requiredcourses_5_0= ruleRequirement ) ) (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )* otherlv_8= '}' )? (otherlv_9= 'reductioncourses' otherlv_10= '{' ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) ) (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) )
            // InternalMyDsl.g:967:2: ( () otherlv_1= 'RelatedCourses' otherlv_2= '{' (otherlv_3= 'requiredcourses' otherlv_4= '{' ( (lv_requiredcourses_5_0= ruleRequirement ) ) (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )* otherlv_8= '}' )? (otherlv_9= 'reductioncourses' otherlv_10= '{' ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) ) (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            {
            // InternalMyDsl.g:967:2: ( () otherlv_1= 'RelatedCourses' otherlv_2= '{' (otherlv_3= 'requiredcourses' otherlv_4= '{' ( (lv_requiredcourses_5_0= ruleRequirement ) ) (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )* otherlv_8= '}' )? (otherlv_9= 'reductioncourses' otherlv_10= '{' ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) ) (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            // InternalMyDsl.g:968:3: () otherlv_1= 'RelatedCourses' otherlv_2= '{' (otherlv_3= 'requiredcourses' otherlv_4= '{' ( (lv_requiredcourses_5_0= ruleRequirement ) ) (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )* otherlv_8= '}' )? (otherlv_9= 'reductioncourses' otherlv_10= '{' ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) ) (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )* otherlv_14= '}' )? otherlv_15= '}'
            {
            // InternalMyDsl.g:968:3: ()
            // InternalMyDsl.g:969:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRelatedCoursesAccess().getRelatedCoursesAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,42,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getRelatedCoursesAccess().getRelatedCoursesKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_39); 

            			newLeafNode(otherlv_2, grammarAccess.getRelatedCoursesAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:983:3: (otherlv_3= 'requiredcourses' otherlv_4= '{' ( (lv_requiredcourses_5_0= ruleRequirement ) ) (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )* otherlv_8= '}' )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==43) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalMyDsl.g:984:4: otherlv_3= 'requiredcourses' otherlv_4= '{' ( (lv_requiredcourses_5_0= ruleRequirement ) ) (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )* otherlv_8= '}'
                    {
                    otherlv_3=(Token)match(input,43,FOLLOW_4); 

                    				newLeafNode(otherlv_3, grammarAccess.getRelatedCoursesAccess().getRequiredcoursesKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,12,FOLLOW_40); 

                    				newLeafNode(otherlv_4, grammarAccess.getRelatedCoursesAccess().getLeftCurlyBracketKeyword_3_1());
                    			
                    // InternalMyDsl.g:992:4: ( (lv_requiredcourses_5_0= ruleRequirement ) )
                    // InternalMyDsl.g:993:5: (lv_requiredcourses_5_0= ruleRequirement )
                    {
                    // InternalMyDsl.g:993:5: (lv_requiredcourses_5_0= ruleRequirement )
                    // InternalMyDsl.g:994:6: lv_requiredcourses_5_0= ruleRequirement
                    {

                    						newCompositeNode(grammarAccess.getRelatedCoursesAccess().getRequiredcoursesRequirementParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_requiredcourses_5_0=ruleRequirement();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRelatedCoursesRule());
                    						}
                    						add(
                    							current,
                    							"requiredcourses",
                    							lv_requiredcourses_5_0,
                    							"coursewebxtext.MyDsl.Requirement");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:1011:4: (otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) ) )*
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==15) ) {
                            alt28=1;
                        }


                        switch (alt28) {
                    	case 1 :
                    	    // InternalMyDsl.g:1012:5: otherlv_6= ',' ( (lv_requiredcourses_7_0= ruleRequirement ) )
                    	    {
                    	    otherlv_6=(Token)match(input,15,FOLLOW_40); 

                    	    					newLeafNode(otherlv_6, grammarAccess.getRelatedCoursesAccess().getCommaKeyword_3_3_0());
                    	    				
                    	    // InternalMyDsl.g:1016:5: ( (lv_requiredcourses_7_0= ruleRequirement ) )
                    	    // InternalMyDsl.g:1017:6: (lv_requiredcourses_7_0= ruleRequirement )
                    	    {
                    	    // InternalMyDsl.g:1017:6: (lv_requiredcourses_7_0= ruleRequirement )
                    	    // InternalMyDsl.g:1018:7: lv_requiredcourses_7_0= ruleRequirement
                    	    {

                    	    							newCompositeNode(grammarAccess.getRelatedCoursesAccess().getRequiredcoursesRequirementParserRuleCall_3_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_requiredcourses_7_0=ruleRequirement();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getRelatedCoursesRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"requiredcourses",
                    	    								lv_requiredcourses_7_0,
                    	    								"coursewebxtext.MyDsl.Requirement");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,18,FOLLOW_41); 

                    				newLeafNode(otherlv_8, grammarAccess.getRelatedCoursesAccess().getRightCurlyBracketKeyword_3_4());
                    			

                    }
                    break;

            }

            // InternalMyDsl.g:1041:3: (otherlv_9= 'reductioncourses' otherlv_10= '{' ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) ) (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )* otherlv_14= '}' )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==44) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalMyDsl.g:1042:4: otherlv_9= 'reductioncourses' otherlv_10= '{' ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) ) (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )* otherlv_14= '}'
                    {
                    otherlv_9=(Token)match(input,44,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getRelatedCoursesAccess().getReductioncoursesKeyword_4_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_42); 

                    				newLeafNode(otherlv_10, grammarAccess.getRelatedCoursesAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalMyDsl.g:1050:4: ( (lv_reductioncourses_11_0= ruleCreditReductionCourse ) )
                    // InternalMyDsl.g:1051:5: (lv_reductioncourses_11_0= ruleCreditReductionCourse )
                    {
                    // InternalMyDsl.g:1051:5: (lv_reductioncourses_11_0= ruleCreditReductionCourse )
                    // InternalMyDsl.g:1052:6: lv_reductioncourses_11_0= ruleCreditReductionCourse
                    {

                    						newCompositeNode(grammarAccess.getRelatedCoursesAccess().getReductioncoursesCreditReductionCourseParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_reductioncourses_11_0=ruleCreditReductionCourse();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRelatedCoursesRule());
                    						}
                    						add(
                    							current,
                    							"reductioncourses",
                    							lv_reductioncourses_11_0,
                    							"coursewebxtext.MyDsl.CreditReductionCourse");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:1069:4: (otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) ) )*
                    loop30:
                    do {
                        int alt30=2;
                        int LA30_0 = input.LA(1);

                        if ( (LA30_0==15) ) {
                            alt30=1;
                        }


                        switch (alt30) {
                    	case 1 :
                    	    // InternalMyDsl.g:1070:5: otherlv_12= ',' ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) )
                    	    {
                    	    otherlv_12=(Token)match(input,15,FOLLOW_42); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getRelatedCoursesAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalMyDsl.g:1074:5: ( (lv_reductioncourses_13_0= ruleCreditReductionCourse ) )
                    	    // InternalMyDsl.g:1075:6: (lv_reductioncourses_13_0= ruleCreditReductionCourse )
                    	    {
                    	    // InternalMyDsl.g:1075:6: (lv_reductioncourses_13_0= ruleCreditReductionCourse )
                    	    // InternalMyDsl.g:1076:7: lv_reductioncourses_13_0= ruleCreditReductionCourse
                    	    {

                    	    							newCompositeNode(grammarAccess.getRelatedCoursesAccess().getReductioncoursesCreditReductionCourseParserRuleCall_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_reductioncourses_13_0=ruleCreditReductionCourse();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getRelatedCoursesRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"reductioncourses",
                    	    								lv_reductioncourses_13_0,
                    	    								"coursewebxtext.MyDsl.CreditReductionCourse");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop30;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,18,FOLLOW_11); 

                    				newLeafNode(otherlv_14, grammarAccess.getRelatedCoursesAccess().getRightCurlyBracketKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getRelatedCoursesAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelatedCourses"


    // $ANTLR start "entryRuleEvaluationForm"
    // InternalMyDsl.g:1107:1: entryRuleEvaluationForm returns [EObject current=null] : iv_ruleEvaluationForm= ruleEvaluationForm EOF ;
    public final EObject entryRuleEvaluationForm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvaluationForm = null;


        try {
            // InternalMyDsl.g:1107:55: (iv_ruleEvaluationForm= ruleEvaluationForm EOF )
            // InternalMyDsl.g:1108:2: iv_ruleEvaluationForm= ruleEvaluationForm EOF
            {
             newCompositeNode(grammarAccess.getEvaluationFormRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEvaluationForm=ruleEvaluationForm();

            state._fsp--;

             current =iv_ruleEvaluationForm; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvaluationForm"


    // $ANTLR start "ruleEvaluationForm"
    // InternalMyDsl.g:1114:1: ruleEvaluationForm returns [EObject current=null] : ( () otherlv_1= 'EvaluationForm' otherlv_2= '{' (otherlv_3= 'Work' otherlv_4= '{' ( (lv_Work_5_0= ruleWork ) ) (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )* otherlv_8= '}' )? otherlv_9= '}' ) ;
    public final EObject ruleEvaluationForm() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        EObject lv_Work_5_0 = null;

        EObject lv_Work_7_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:1120:2: ( ( () otherlv_1= 'EvaluationForm' otherlv_2= '{' (otherlv_3= 'Work' otherlv_4= '{' ( (lv_Work_5_0= ruleWork ) ) (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )* otherlv_8= '}' )? otherlv_9= '}' ) )
            // InternalMyDsl.g:1121:2: ( () otherlv_1= 'EvaluationForm' otherlv_2= '{' (otherlv_3= 'Work' otherlv_4= '{' ( (lv_Work_5_0= ruleWork ) ) (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )* otherlv_8= '}' )? otherlv_9= '}' )
            {
            // InternalMyDsl.g:1121:2: ( () otherlv_1= 'EvaluationForm' otherlv_2= '{' (otherlv_3= 'Work' otherlv_4= '{' ( (lv_Work_5_0= ruleWork ) ) (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )* otherlv_8= '}' )? otherlv_9= '}' )
            // InternalMyDsl.g:1122:3: () otherlv_1= 'EvaluationForm' otherlv_2= '{' (otherlv_3= 'Work' otherlv_4= '{' ( (lv_Work_5_0= ruleWork ) ) (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )* otherlv_8= '}' )? otherlv_9= '}'
            {
            // InternalMyDsl.g:1122:3: ()
            // InternalMyDsl.g:1123:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEvaluationFormAccess().getEvaluationFormAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,45,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getEvaluationFormAccess().getEvaluationFormKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_43); 

            			newLeafNode(otherlv_2, grammarAccess.getEvaluationFormAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:1137:3: (otherlv_3= 'Work' otherlv_4= '{' ( (lv_Work_5_0= ruleWork ) ) (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )* otherlv_8= '}' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==46) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalMyDsl.g:1138:4: otherlv_3= 'Work' otherlv_4= '{' ( (lv_Work_5_0= ruleWork ) ) (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )* otherlv_8= '}'
                    {
                    otherlv_3=(Token)match(input,46,FOLLOW_4); 

                    				newLeafNode(otherlv_3, grammarAccess.getEvaluationFormAccess().getWorkKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,12,FOLLOW_44); 

                    				newLeafNode(otherlv_4, grammarAccess.getEvaluationFormAccess().getLeftCurlyBracketKeyword_3_1());
                    			
                    // InternalMyDsl.g:1146:4: ( (lv_Work_5_0= ruleWork ) )
                    // InternalMyDsl.g:1147:5: (lv_Work_5_0= ruleWork )
                    {
                    // InternalMyDsl.g:1147:5: (lv_Work_5_0= ruleWork )
                    // InternalMyDsl.g:1148:6: lv_Work_5_0= ruleWork
                    {

                    						newCompositeNode(grammarAccess.getEvaluationFormAccess().getWorkWorkParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_Work_5_0=ruleWork();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEvaluationFormRule());
                    						}
                    						add(
                    							current,
                    							"Work",
                    							lv_Work_5_0,
                    							"coursewebxtext.MyDsl.Work");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:1165:4: (otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==15) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalMyDsl.g:1166:5: otherlv_6= ',' ( (lv_Work_7_0= ruleWork ) )
                    	    {
                    	    otherlv_6=(Token)match(input,15,FOLLOW_44); 

                    	    					newLeafNode(otherlv_6, grammarAccess.getEvaluationFormAccess().getCommaKeyword_3_3_0());
                    	    				
                    	    // InternalMyDsl.g:1170:5: ( (lv_Work_7_0= ruleWork ) )
                    	    // InternalMyDsl.g:1171:6: (lv_Work_7_0= ruleWork )
                    	    {
                    	    // InternalMyDsl.g:1171:6: (lv_Work_7_0= ruleWork )
                    	    // InternalMyDsl.g:1172:7: lv_Work_7_0= ruleWork
                    	    {

                    	    							newCompositeNode(grammarAccess.getEvaluationFormAccess().getWorkWorkParserRuleCall_3_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_Work_7_0=ruleWork();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getEvaluationFormRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"Work",
                    	    								lv_Work_7_0,
                    	    								"coursewebxtext.MyDsl.Work");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,18,FOLLOW_11); 

                    				newLeafNode(otherlv_8, grammarAccess.getEvaluationFormAccess().getRightCurlyBracketKeyword_3_4());
                    			

                    }
                    break;

            }

            otherlv_9=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getEvaluationFormAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvaluationForm"


    // $ANTLR start "entryRuleOrganization"
    // InternalMyDsl.g:1203:1: entryRuleOrganization returns [EObject current=null] : iv_ruleOrganization= ruleOrganization EOF ;
    public final EObject entryRuleOrganization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrganization = null;


        try {
            // InternalMyDsl.g:1203:53: (iv_ruleOrganization= ruleOrganization EOF )
            // InternalMyDsl.g:1204:2: iv_ruleOrganization= ruleOrganization EOF
            {
             newCompositeNode(grammarAccess.getOrganizationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrganization=ruleOrganization();

            state._fsp--;

             current =iv_ruleOrganization; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrganization"


    // $ANTLR start "ruleOrganization"
    // InternalMyDsl.g:1210:1: ruleOrganization returns [EObject current=null] : ( () otherlv_1= 'Organization' otherlv_2= '{' (otherlv_3= 'department' ( (lv_department_4_0= ruleEString ) ) )? (otherlv_5= 'people' otherlv_6= '{' ( (lv_people_7_0= rulePeople ) ) (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )* otherlv_10= '}' )? otherlv_11= '}' ) ;
    public final EObject ruleOrganization() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_department_4_0 = null;

        EObject lv_people_7_0 = null;

        EObject lv_people_9_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:1216:2: ( ( () otherlv_1= 'Organization' otherlv_2= '{' (otherlv_3= 'department' ( (lv_department_4_0= ruleEString ) ) )? (otherlv_5= 'people' otherlv_6= '{' ( (lv_people_7_0= rulePeople ) ) (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )* otherlv_10= '}' )? otherlv_11= '}' ) )
            // InternalMyDsl.g:1217:2: ( () otherlv_1= 'Organization' otherlv_2= '{' (otherlv_3= 'department' ( (lv_department_4_0= ruleEString ) ) )? (otherlv_5= 'people' otherlv_6= '{' ( (lv_people_7_0= rulePeople ) ) (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )* otherlv_10= '}' )? otherlv_11= '}' )
            {
            // InternalMyDsl.g:1217:2: ( () otherlv_1= 'Organization' otherlv_2= '{' (otherlv_3= 'department' ( (lv_department_4_0= ruleEString ) ) )? (otherlv_5= 'people' otherlv_6= '{' ( (lv_people_7_0= rulePeople ) ) (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )* otherlv_10= '}' )? otherlv_11= '}' )
            // InternalMyDsl.g:1218:3: () otherlv_1= 'Organization' otherlv_2= '{' (otherlv_3= 'department' ( (lv_department_4_0= ruleEString ) ) )? (otherlv_5= 'people' otherlv_6= '{' ( (lv_people_7_0= rulePeople ) ) (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )* otherlv_10= '}' )? otherlv_11= '}'
            {
            // InternalMyDsl.g:1218:3: ()
            // InternalMyDsl.g:1219:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getOrganizationAccess().getOrganizationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,47,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getOrganizationAccess().getOrganizationKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_45); 

            			newLeafNode(otherlv_2, grammarAccess.getOrganizationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:1233:3: (otherlv_3= 'department' ( (lv_department_4_0= ruleEString ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==48) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalMyDsl.g:1234:4: otherlv_3= 'department' ( (lv_department_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,48,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getOrganizationAccess().getDepartmentKeyword_3_0());
                    			
                    // InternalMyDsl.g:1238:4: ( (lv_department_4_0= ruleEString ) )
                    // InternalMyDsl.g:1239:5: (lv_department_4_0= ruleEString )
                    {
                    // InternalMyDsl.g:1239:5: (lv_department_4_0= ruleEString )
                    // InternalMyDsl.g:1240:6: lv_department_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOrganizationAccess().getDepartmentEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_46);
                    lv_department_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOrganizationRule());
                    						}
                    						set(
                    							current,
                    							"department",
                    							lv_department_4_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:1258:3: (otherlv_5= 'people' otherlv_6= '{' ( (lv_people_7_0= rulePeople ) ) (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )* otherlv_10= '}' )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==49) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalMyDsl.g:1259:4: otherlv_5= 'people' otherlv_6= '{' ( (lv_people_7_0= rulePeople ) ) (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )* otherlv_10= '}'
                    {
                    otherlv_5=(Token)match(input,49,FOLLOW_4); 

                    				newLeafNode(otherlv_5, grammarAccess.getOrganizationAccess().getPeopleKeyword_4_0());
                    			
                    otherlv_6=(Token)match(input,12,FOLLOW_47); 

                    				newLeafNode(otherlv_6, grammarAccess.getOrganizationAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalMyDsl.g:1267:4: ( (lv_people_7_0= rulePeople ) )
                    // InternalMyDsl.g:1268:5: (lv_people_7_0= rulePeople )
                    {
                    // InternalMyDsl.g:1268:5: (lv_people_7_0= rulePeople )
                    // InternalMyDsl.g:1269:6: lv_people_7_0= rulePeople
                    {

                    						newCompositeNode(grammarAccess.getOrganizationAccess().getPeoplePeopleParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_people_7_0=rulePeople();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOrganizationRule());
                    						}
                    						add(
                    							current,
                    							"people",
                    							lv_people_7_0,
                    							"coursewebxtext.MyDsl.People");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:1286:4: (otherlv_8= ',' ( (lv_people_9_0= rulePeople ) ) )*
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( (LA35_0==15) ) {
                            alt35=1;
                        }


                        switch (alt35) {
                    	case 1 :
                    	    // InternalMyDsl.g:1287:5: otherlv_8= ',' ( (lv_people_9_0= rulePeople ) )
                    	    {
                    	    otherlv_8=(Token)match(input,15,FOLLOW_47); 

                    	    					newLeafNode(otherlv_8, grammarAccess.getOrganizationAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalMyDsl.g:1291:5: ( (lv_people_9_0= rulePeople ) )
                    	    // InternalMyDsl.g:1292:6: (lv_people_9_0= rulePeople )
                    	    {
                    	    // InternalMyDsl.g:1292:6: (lv_people_9_0= rulePeople )
                    	    // InternalMyDsl.g:1293:7: lv_people_9_0= rulePeople
                    	    {

                    	    							newCompositeNode(grammarAccess.getOrganizationAccess().getPeoplePeopleParserRuleCall_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_people_9_0=rulePeople();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getOrganizationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"people",
                    	    								lv_people_9_0,
                    	    								"coursewebxtext.MyDsl.People");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop35;
                        }
                    } while (true);

                    otherlv_10=(Token)match(input,18,FOLLOW_11); 

                    				newLeafNode(otherlv_10, grammarAccess.getOrganizationAccess().getRightCurlyBracketKeyword_4_4());
                    			

                    }
                    break;

            }

            otherlv_11=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getOrganizationAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrganization"


    // $ANTLR start "entryRuleTimetable"
    // InternalMyDsl.g:1324:1: entryRuleTimetable returns [EObject current=null] : iv_ruleTimetable= ruleTimetable EOF ;
    public final EObject entryRuleTimetable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimetable = null;


        try {
            // InternalMyDsl.g:1324:50: (iv_ruleTimetable= ruleTimetable EOF )
            // InternalMyDsl.g:1325:2: iv_ruleTimetable= ruleTimetable EOF
            {
             newCompositeNode(grammarAccess.getTimetableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimetable=ruleTimetable();

            state._fsp--;

             current =iv_ruleTimetable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimetable"


    // $ANTLR start "ruleTimetable"
    // InternalMyDsl.g:1331:1: ruleTimetable returns [EObject current=null] : ( () otherlv_1= 'Timetable' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? (otherlv_7= 'schedule' otherlv_8= '{' ( (lv_schedule_9_0= ruleSchedule ) ) (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )* otherlv_12= '}' )? otherlv_13= '}' ) ;
    public final EObject ruleTimetable() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_lectureHours_4_0 = null;

        AntlrDatatypeRuleToken lv_labHours_6_0 = null;

        EObject lv_schedule_9_0 = null;

        EObject lv_schedule_11_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:1337:2: ( ( () otherlv_1= 'Timetable' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? (otherlv_7= 'schedule' otherlv_8= '{' ( (lv_schedule_9_0= ruleSchedule ) ) (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )* otherlv_12= '}' )? otherlv_13= '}' ) )
            // InternalMyDsl.g:1338:2: ( () otherlv_1= 'Timetable' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? (otherlv_7= 'schedule' otherlv_8= '{' ( (lv_schedule_9_0= ruleSchedule ) ) (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )* otherlv_12= '}' )? otherlv_13= '}' )
            {
            // InternalMyDsl.g:1338:2: ( () otherlv_1= 'Timetable' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? (otherlv_7= 'schedule' otherlv_8= '{' ( (lv_schedule_9_0= ruleSchedule ) ) (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )* otherlv_12= '}' )? otherlv_13= '}' )
            // InternalMyDsl.g:1339:3: () otherlv_1= 'Timetable' otherlv_2= '{' (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )? (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )? (otherlv_7= 'schedule' otherlv_8= '{' ( (lv_schedule_9_0= ruleSchedule ) ) (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )* otherlv_12= '}' )? otherlv_13= '}'
            {
            // InternalMyDsl.g:1339:3: ()
            // InternalMyDsl.g:1340:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTimetableAccess().getTimetableAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,50,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getTimetableAccess().getTimetableKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_48); 

            			newLeafNode(otherlv_2, grammarAccess.getTimetableAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:1354:3: (otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==40) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalMyDsl.g:1355:4: otherlv_3= 'lectureHours' ( (lv_lectureHours_4_0= ruleEDouble ) )
                    {
                    otherlv_3=(Token)match(input,40,FOLLOW_18); 

                    				newLeafNode(otherlv_3, grammarAccess.getTimetableAccess().getLectureHoursKeyword_3_0());
                    			
                    // InternalMyDsl.g:1359:4: ( (lv_lectureHours_4_0= ruleEDouble ) )
                    // InternalMyDsl.g:1360:5: (lv_lectureHours_4_0= ruleEDouble )
                    {
                    // InternalMyDsl.g:1360:5: (lv_lectureHours_4_0= ruleEDouble )
                    // InternalMyDsl.g:1361:6: lv_lectureHours_4_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getTimetableAccess().getLectureHoursEDoubleParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_49);
                    lv_lectureHours_4_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTimetableRule());
                    						}
                    						set(
                    							current,
                    							"lectureHours",
                    							lv_lectureHours_4_0,
                    							"coursewebxtext.MyDsl.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:1379:3: (otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==41) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalMyDsl.g:1380:4: otherlv_5= 'labHours' ( (lv_labHours_6_0= ruleEDouble ) )
                    {
                    otherlv_5=(Token)match(input,41,FOLLOW_18); 

                    				newLeafNode(otherlv_5, grammarAccess.getTimetableAccess().getLabHoursKeyword_4_0());
                    			
                    // InternalMyDsl.g:1384:4: ( (lv_labHours_6_0= ruleEDouble ) )
                    // InternalMyDsl.g:1385:5: (lv_labHours_6_0= ruleEDouble )
                    {
                    // InternalMyDsl.g:1385:5: (lv_labHours_6_0= ruleEDouble )
                    // InternalMyDsl.g:1386:6: lv_labHours_6_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getTimetableAccess().getLabHoursEDoubleParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_50);
                    lv_labHours_6_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTimetableRule());
                    						}
                    						set(
                    							current,
                    							"labHours",
                    							lv_labHours_6_0,
                    							"coursewebxtext.MyDsl.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:1404:3: (otherlv_7= 'schedule' otherlv_8= '{' ( (lv_schedule_9_0= ruleSchedule ) ) (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )* otherlv_12= '}' )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==13) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalMyDsl.g:1405:4: otherlv_7= 'schedule' otherlv_8= '{' ( (lv_schedule_9_0= ruleSchedule ) ) (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )* otherlv_12= '}'
                    {
                    otherlv_7=(Token)match(input,13,FOLLOW_4); 

                    				newLeafNode(otherlv_7, grammarAccess.getTimetableAccess().getScheduleKeyword_5_0());
                    			
                    otherlv_8=(Token)match(input,12,FOLLOW_51); 

                    				newLeafNode(otherlv_8, grammarAccess.getTimetableAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalMyDsl.g:1413:4: ( (lv_schedule_9_0= ruleSchedule ) )
                    // InternalMyDsl.g:1414:5: (lv_schedule_9_0= ruleSchedule )
                    {
                    // InternalMyDsl.g:1414:5: (lv_schedule_9_0= ruleSchedule )
                    // InternalMyDsl.g:1415:6: lv_schedule_9_0= ruleSchedule
                    {

                    						newCompositeNode(grammarAccess.getTimetableAccess().getScheduleScheduleParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_schedule_9_0=ruleSchedule();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTimetableRule());
                    						}
                    						add(
                    							current,
                    							"schedule",
                    							lv_schedule_9_0,
                    							"coursewebxtext.MyDsl.Schedule");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalMyDsl.g:1432:4: (otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) ) )*
                    loop39:
                    do {
                        int alt39=2;
                        int LA39_0 = input.LA(1);

                        if ( (LA39_0==15) ) {
                            alt39=1;
                        }


                        switch (alt39) {
                    	case 1 :
                    	    // InternalMyDsl.g:1433:5: otherlv_10= ',' ( (lv_schedule_11_0= ruleSchedule ) )
                    	    {
                    	    otherlv_10=(Token)match(input,15,FOLLOW_51); 

                    	    					newLeafNode(otherlv_10, grammarAccess.getTimetableAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalMyDsl.g:1437:5: ( (lv_schedule_11_0= ruleSchedule ) )
                    	    // InternalMyDsl.g:1438:6: (lv_schedule_11_0= ruleSchedule )
                    	    {
                    	    // InternalMyDsl.g:1438:6: (lv_schedule_11_0= ruleSchedule )
                    	    // InternalMyDsl.g:1439:7: lv_schedule_11_0= ruleSchedule
                    	    {

                    	    							newCompositeNode(grammarAccess.getTimetableAccess().getScheduleScheduleParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_10);
                    	    lv_schedule_11_0=ruleSchedule();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getTimetableRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"schedule",
                    	    								lv_schedule_11_0,
                    	    								"coursewebxtext.MyDsl.Schedule");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop39;
                        }
                    } while (true);

                    otherlv_12=(Token)match(input,18,FOLLOW_11); 

                    				newLeafNode(otherlv_12, grammarAccess.getTimetableAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            otherlv_13=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_13, grammarAccess.getTimetableAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimetable"


    // $ANTLR start "entryRuleWork"
    // InternalMyDsl.g:1470:1: entryRuleWork returns [EObject current=null] : iv_ruleWork= ruleWork EOF ;
    public final EObject entryRuleWork() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWork = null;


        try {
            // InternalMyDsl.g:1470:45: (iv_ruleWork= ruleWork EOF )
            // InternalMyDsl.g:1471:2: iv_ruleWork= ruleWork EOF
            {
             newCompositeNode(grammarAccess.getWorkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWork=ruleWork();

            state._fsp--;

             current =iv_ruleWork; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWork"


    // $ANTLR start "ruleWork"
    // InternalMyDsl.g:1477:1: ruleWork returns [EObject current=null] : ( () otherlv_1= 'Work' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'porcentage' ( (lv_porcentage_5_0= ruleEDouble ) ) )? otherlv_6= '}' ) ;
    public final EObject ruleWork() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_porcentage_5_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:1483:2: ( ( () otherlv_1= 'Work' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'porcentage' ( (lv_porcentage_5_0= ruleEDouble ) ) )? otherlv_6= '}' ) )
            // InternalMyDsl.g:1484:2: ( () otherlv_1= 'Work' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'porcentage' ( (lv_porcentage_5_0= ruleEDouble ) ) )? otherlv_6= '}' )
            {
            // InternalMyDsl.g:1484:2: ( () otherlv_1= 'Work' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'porcentage' ( (lv_porcentage_5_0= ruleEDouble ) ) )? otherlv_6= '}' )
            // InternalMyDsl.g:1485:3: () otherlv_1= 'Work' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'porcentage' ( (lv_porcentage_5_0= ruleEDouble ) ) )? otherlv_6= '}'
            {
            // InternalMyDsl.g:1485:3: ()
            // InternalMyDsl.g:1486:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getWorkAccess().getWorkAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,46,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getWorkAccess().getWorkKeyword_1());
            		
            // InternalMyDsl.g:1496:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:1497:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:1497:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:1498:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWorkAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"coursewebxtext.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_52); 

            			newLeafNode(otherlv_3, grammarAccess.getWorkAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMyDsl.g:1519:3: (otherlv_4= 'porcentage' ( (lv_porcentage_5_0= ruleEDouble ) ) )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==51) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalMyDsl.g:1520:4: otherlv_4= 'porcentage' ( (lv_porcentage_5_0= ruleEDouble ) )
                    {
                    otherlv_4=(Token)match(input,51,FOLLOW_18); 

                    				newLeafNode(otherlv_4, grammarAccess.getWorkAccess().getPorcentageKeyword_4_0());
                    			
                    // InternalMyDsl.g:1524:4: ( (lv_porcentage_5_0= ruleEDouble ) )
                    // InternalMyDsl.g:1525:5: (lv_porcentage_5_0= ruleEDouble )
                    {
                    // InternalMyDsl.g:1525:5: (lv_porcentage_5_0= ruleEDouble )
                    // InternalMyDsl.g:1526:6: lv_porcentage_5_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getWorkAccess().getPorcentageEDoubleParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_porcentage_5_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkRule());
                    						}
                    						set(
                    							current,
                    							"porcentage",
                    							lv_porcentage_5_0,
                    							"coursewebxtext.MyDsl.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getWorkAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWork"


    // $ANTLR start "entryRulePeople"
    // InternalMyDsl.g:1552:1: entryRulePeople returns [EObject current=null] : iv_rulePeople= rulePeople EOF ;
    public final EObject entryRulePeople() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePeople = null;


        try {
            // InternalMyDsl.g:1552:47: (iv_rulePeople= rulePeople EOF )
            // InternalMyDsl.g:1553:2: iv_rulePeople= rulePeople EOF
            {
             newCompositeNode(grammarAccess.getPeopleRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePeople=rulePeople();

            state._fsp--;

             current =iv_rulePeople; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePeople"


    // $ANTLR start "rulePeople"
    // InternalMyDsl.g:1559:1: rulePeople returns [EObject current=null] : ( () otherlv_1= 'People' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'Role' ( (lv_Role_5_0= ruleEString ) ) )? otherlv_6= '}' ) ;
    public final EObject rulePeople() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_Role_5_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:1565:2: ( ( () otherlv_1= 'People' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'Role' ( (lv_Role_5_0= ruleEString ) ) )? otherlv_6= '}' ) )
            // InternalMyDsl.g:1566:2: ( () otherlv_1= 'People' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'Role' ( (lv_Role_5_0= ruleEString ) ) )? otherlv_6= '}' )
            {
            // InternalMyDsl.g:1566:2: ( () otherlv_1= 'People' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'Role' ( (lv_Role_5_0= ruleEString ) ) )? otherlv_6= '}' )
            // InternalMyDsl.g:1567:3: () otherlv_1= 'People' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'Role' ( (lv_Role_5_0= ruleEString ) ) )? otherlv_6= '}'
            {
            // InternalMyDsl.g:1567:3: ()
            // InternalMyDsl.g:1568:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPeopleAccess().getPeopleAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,52,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPeopleAccess().getPeopleKeyword_1());
            		
            // InternalMyDsl.g:1578:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:1579:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:1579:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:1580:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPeopleAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPeopleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"coursewebxtext.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_53); 

            			newLeafNode(otherlv_3, grammarAccess.getPeopleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMyDsl.g:1601:3: (otherlv_4= 'Role' ( (lv_Role_5_0= ruleEString ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==53) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalMyDsl.g:1602:4: otherlv_4= 'Role' ( (lv_Role_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,53,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getPeopleAccess().getRoleKeyword_4_0());
                    			
                    // InternalMyDsl.g:1606:4: ( (lv_Role_5_0= ruleEString ) )
                    // InternalMyDsl.g:1607:5: (lv_Role_5_0= ruleEString )
                    {
                    // InternalMyDsl.g:1607:5: (lv_Role_5_0= ruleEString )
                    // InternalMyDsl.g:1608:6: lv_Role_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPeopleAccess().getRoleEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_Role_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPeopleRule());
                    						}
                    						set(
                    							current,
                    							"Role",
                    							lv_Role_5_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getPeopleAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePeople"


    // $ANTLR start "entryRuleRequirement"
    // InternalMyDsl.g:1634:1: entryRuleRequirement returns [EObject current=null] : iv_ruleRequirement= ruleRequirement EOF ;
    public final EObject entryRuleRequirement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequirement = null;


        try {
            // InternalMyDsl.g:1634:52: (iv_ruleRequirement= ruleRequirement EOF )
            // InternalMyDsl.g:1635:2: iv_ruleRequirement= ruleRequirement EOF
            {
             newCompositeNode(grammarAccess.getRequirementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequirement=ruleRequirement();

            state._fsp--;

             current =iv_ruleRequirement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequirement"


    // $ANTLR start "ruleRequirement"
    // InternalMyDsl.g:1641:1: ruleRequirement returns [EObject current=null] : ( () otherlv_1= 'Requirement' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'TypeOfRequierement' ( (lv_TypeOfRequierement_7_0= ruleEString ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleRequirement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_code_5_0 = null;

        AntlrDatatypeRuleToken lv_TypeOfRequierement_7_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:1647:2: ( ( () otherlv_1= 'Requirement' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'TypeOfRequierement' ( (lv_TypeOfRequierement_7_0= ruleEString ) ) )? otherlv_8= '}' ) )
            // InternalMyDsl.g:1648:2: ( () otherlv_1= 'Requirement' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'TypeOfRequierement' ( (lv_TypeOfRequierement_7_0= ruleEString ) ) )? otherlv_8= '}' )
            {
            // InternalMyDsl.g:1648:2: ( () otherlv_1= 'Requirement' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'TypeOfRequierement' ( (lv_TypeOfRequierement_7_0= ruleEString ) ) )? otherlv_8= '}' )
            // InternalMyDsl.g:1649:3: () otherlv_1= 'Requirement' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )? (otherlv_6= 'TypeOfRequierement' ( (lv_TypeOfRequierement_7_0= ruleEString ) ) )? otherlv_8= '}'
            {
            // InternalMyDsl.g:1649:3: ()
            // InternalMyDsl.g:1650:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRequirementAccess().getRequirementAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,54,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getRequirementAccess().getRequirementKeyword_1());
            		
            // InternalMyDsl.g:1660:3: ( (lv_name_2_0= ruleEString ) )
            // InternalMyDsl.g:1661:4: (lv_name_2_0= ruleEString )
            {
            // InternalMyDsl.g:1661:4: (lv_name_2_0= ruleEString )
            // InternalMyDsl.g:1662:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRequirementAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRequirementRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"coursewebxtext.MyDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_54); 

            			newLeafNode(otherlv_3, grammarAccess.getRequirementAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalMyDsl.g:1683:3: (otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==24) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalMyDsl.g:1684:4: otherlv_4= 'code' ( (lv_code_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getRequirementAccess().getCodeKeyword_4_0());
                    			
                    // InternalMyDsl.g:1688:4: ( (lv_code_5_0= ruleEString ) )
                    // InternalMyDsl.g:1689:5: (lv_code_5_0= ruleEString )
                    {
                    // InternalMyDsl.g:1689:5: (lv_code_5_0= ruleEString )
                    // InternalMyDsl.g:1690:6: lv_code_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getRequirementAccess().getCodeEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_55);
                    lv_code_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRequirementRule());
                    						}
                    						set(
                    							current,
                    							"code",
                    							lv_code_5_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:1708:3: (otherlv_6= 'TypeOfRequierement' ( (lv_TypeOfRequierement_7_0= ruleEString ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==55) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalMyDsl.g:1709:4: otherlv_6= 'TypeOfRequierement' ( (lv_TypeOfRequierement_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,55,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getRequirementAccess().getTypeOfRequierementKeyword_5_0());
                    			
                    // InternalMyDsl.g:1713:4: ( (lv_TypeOfRequierement_7_0= ruleEString ) )
                    // InternalMyDsl.g:1714:5: (lv_TypeOfRequierement_7_0= ruleEString )
                    {
                    // InternalMyDsl.g:1714:5: (lv_TypeOfRequierement_7_0= ruleEString )
                    // InternalMyDsl.g:1715:6: lv_TypeOfRequierement_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getRequirementAccess().getTypeOfRequierementEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_TypeOfRequierement_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRequirementRule());
                    						}
                    						set(
                    							current,
                    							"TypeOfRequierement",
                    							lv_TypeOfRequierement_7_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getRequirementAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequirement"


    // $ANTLR start "entryRuleCreditReductionCourse"
    // InternalMyDsl.g:1741:1: entryRuleCreditReductionCourse returns [EObject current=null] : iv_ruleCreditReductionCourse= ruleCreditReductionCourse EOF ;
    public final EObject entryRuleCreditReductionCourse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCreditReductionCourse = null;


        try {
            // InternalMyDsl.g:1741:62: (iv_ruleCreditReductionCourse= ruleCreditReductionCourse EOF )
            // InternalMyDsl.g:1742:2: iv_ruleCreditReductionCourse= ruleCreditReductionCourse EOF
            {
             newCompositeNode(grammarAccess.getCreditReductionCourseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCreditReductionCourse=ruleCreditReductionCourse();

            state._fsp--;

             current =iv_ruleCreditReductionCourse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCreditReductionCourse"


    // $ANTLR start "ruleCreditReductionCourse"
    // InternalMyDsl.g:1748:1: ruleCreditReductionCourse returns [EObject current=null] : ( () otherlv_1= 'CreditReductionCourse' otherlv_2= '{' (otherlv_3= 'code' ( (lv_code_4_0= ruleEString ) ) )? (otherlv_5= 'creditReduction' ( (lv_creditReduction_6_0= ruleEDouble ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleCreditReductionCourse() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_code_4_0 = null;

        AntlrDatatypeRuleToken lv_creditReduction_6_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:1754:2: ( ( () otherlv_1= 'CreditReductionCourse' otherlv_2= '{' (otherlv_3= 'code' ( (lv_code_4_0= ruleEString ) ) )? (otherlv_5= 'creditReduction' ( (lv_creditReduction_6_0= ruleEDouble ) ) )? otherlv_7= '}' ) )
            // InternalMyDsl.g:1755:2: ( () otherlv_1= 'CreditReductionCourse' otherlv_2= '{' (otherlv_3= 'code' ( (lv_code_4_0= ruleEString ) ) )? (otherlv_5= 'creditReduction' ( (lv_creditReduction_6_0= ruleEDouble ) ) )? otherlv_7= '}' )
            {
            // InternalMyDsl.g:1755:2: ( () otherlv_1= 'CreditReductionCourse' otherlv_2= '{' (otherlv_3= 'code' ( (lv_code_4_0= ruleEString ) ) )? (otherlv_5= 'creditReduction' ( (lv_creditReduction_6_0= ruleEDouble ) ) )? otherlv_7= '}' )
            // InternalMyDsl.g:1756:3: () otherlv_1= 'CreditReductionCourse' otherlv_2= '{' (otherlv_3= 'code' ( (lv_code_4_0= ruleEString ) ) )? (otherlv_5= 'creditReduction' ( (lv_creditReduction_6_0= ruleEDouble ) ) )? otherlv_7= '}'
            {
            // InternalMyDsl.g:1756:3: ()
            // InternalMyDsl.g:1757:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCreditReductionCourseAccess().getCreditReductionCourseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,56,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getCreditReductionCourseAccess().getCreditReductionCourseKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_56); 

            			newLeafNode(otherlv_2, grammarAccess.getCreditReductionCourseAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalMyDsl.g:1771:3: (otherlv_3= 'code' ( (lv_code_4_0= ruleEString ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==24) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalMyDsl.g:1772:4: otherlv_3= 'code' ( (lv_code_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getCreditReductionCourseAccess().getCodeKeyword_3_0());
                    			
                    // InternalMyDsl.g:1776:4: ( (lv_code_4_0= ruleEString ) )
                    // InternalMyDsl.g:1777:5: (lv_code_4_0= ruleEString )
                    {
                    // InternalMyDsl.g:1777:5: (lv_code_4_0= ruleEString )
                    // InternalMyDsl.g:1778:6: lv_code_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCreditReductionCourseAccess().getCodeEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_57);
                    lv_code_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCreditReductionCourseRule());
                    						}
                    						set(
                    							current,
                    							"code",
                    							lv_code_4_0,
                    							"coursewebxtext.MyDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalMyDsl.g:1796:3: (otherlv_5= 'creditReduction' ( (lv_creditReduction_6_0= ruleEDouble ) ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==57) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalMyDsl.g:1797:4: otherlv_5= 'creditReduction' ( (lv_creditReduction_6_0= ruleEDouble ) )
                    {
                    otherlv_5=(Token)match(input,57,FOLLOW_18); 

                    				newLeafNode(otherlv_5, grammarAccess.getCreditReductionCourseAccess().getCreditReductionKeyword_4_0());
                    			
                    // InternalMyDsl.g:1801:4: ( (lv_creditReduction_6_0= ruleEDouble ) )
                    // InternalMyDsl.g:1802:5: (lv_creditReduction_6_0= ruleEDouble )
                    {
                    // InternalMyDsl.g:1802:5: (lv_creditReduction_6_0= ruleEDouble )
                    // InternalMyDsl.g:1803:6: lv_creditReduction_6_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getCreditReductionCourseAccess().getCreditReductionEDoubleParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_creditReduction_6_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCreditReductionCourseRule());
                    						}
                    						set(
                    							current,
                    							"creditReduction",
                    							lv_creditReduction_6_0,
                    							"coursewebxtext.MyDsl.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getCreditReductionCourseAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCreditReductionCourse"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000062000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000048000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000740000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000640000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000440000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x000000003F040000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000003E040000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x000000003C040000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000001800000040L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000038040000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000030040000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020040000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000780040000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000700040000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000600040000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000400040000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000001000000040L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000006000000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000800000040L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000030000040000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000020000040000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000180000040000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000100000040000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000400000040000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0003000000040000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0002000000040000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000030000042000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000020000042000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000042000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0008000000040000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0020000000040000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0080000001040000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0080000000040000L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0200000001040000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0200000000040000L});

}